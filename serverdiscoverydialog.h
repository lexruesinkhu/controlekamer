#ifndef SERVERDISCOVERYDIALOG_H
#define SERVERDISCOVERYDIALOG_H

#include <QDialog>
#include <QtNetwork/QUdpSocket>

namespace Ui {
class ServerDiscoveryDialog;
}

class ServerDiscoveryDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ServerDiscoveryDialog(QWidget *parent = 0);
    ~ServerDiscoveryDialog();

    QString serverIp()
    {
        return chosenIp;
    }

private slots:
    void broadcastDatagram();
    void receiveDatagramResponse();
    void chooseServer();
    void enableConnectButton();

private:
    Ui::ServerDiscoveryDialog *ui;

    QUdpSocket *socket;
    QString chosenIp;

    bool found = false;
};

#endif // SERVERDISCOVERYDIALOG_H
