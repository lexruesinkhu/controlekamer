#ifndef EVENT_H
#define EVENT_H

enum EventType : int {
    EMERGENCY = 0
};

class Event {
public:
    int event_nr = -1;
    int event_type = -1;
    int room_nr = -1;

    Event(int event_type, int room_nr)
        : event_type(event_type), room_nr(room_nr) { }

    Event(int event_nr, int event_type, int room_nr)
            : event_nr(event_nr), event_type(event_type), room_nr(room_nr) { }


    static Event fromString(QString const& source)
    {
        auto split = source.split(";");

        int event_nr = split[0].toInt();
        int event_type = split[1].toInt();
        int room_nr = split[2].toInt();

       return Event(event_nr, event_type, room_nr);
    }
};


#endif // EVENT_H
