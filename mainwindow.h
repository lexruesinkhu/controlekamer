#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QVBoxLayout>
#include <QStringListModel>

#include "serverdiscoverydialog.h"
#include "server.h"

#include "clientview.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void changeStatusMessage(QString text);

public slots:
    void resetEmergency();
    void fetchClients();
    void fetchEvents();
    void processNetworkEvent(Packet packet);

private slots:
    void startDiscoveryDialog();    

private:
    QString createEventText(Event& event);
    void addEventToList(Event& event);

    QVBoxLayout *rightLayout;
    QLabel *emergency;

    Ui::MainWindow *ui;
    ServerDiscoveryDialog *discoveryDialog;
    Server *server;
    ServerThread *serverThread;

    bool discoveryRejected = false;
};

#endif // MAINWINDOW_H
