#ifndef PACKET_H
#define PACKET_H

#include <QVariant>
#include <QString>
#include <QUuid>
#include <QRegExp>

enum PacketType : int {
    DISCOVERY = 0,
    DISCOVERY_ANSWER = 1,
    STATE_CHANGE = 2,
    STATE_FETCH = 3,
    STATE_RESPONSE = 4,
    CONTROL = 5,
    KEEPALIVE = 6
};

enum StateChange : int {
    LIGHT_TURNED_ON = 0,
    LIGHT_TURNED_OFF = 1,
    CAMERA_ENABLED = 2,
    CAMERA_DISABLED = 3,
    START_EMERGENCY = 4
};

enum StateFetch : int {
    GET_CLIENTS = 0,
    GET_EVENTS = 1,
    GET_IP = 2
};

enum ControlType : int {
    GET_ID = 0,
    REGISTER_CONTROLLER = 1,
    TURN_LIGHT_ON = 2,
    REGISTER_APP = 3
};

class Packet {
public:
    int source;
    int destination;
    int type;
    int instruction;

    QString conversationId;
    QString data;

    static Packet make(int destination, int packet_type) {
        return make(destination, packet_type, 0);
    }

    static Packet make(int destination, int packet_type, int instruction) {
        return make(destination, packet_type, instruction, "");
    }

    static Packet make(int destination, int packet_type, int instruction, QString data) {
        Packet p;
        p.conversationId = createConversationId();
        p.source = 0;
        p.destination = destination;
        p.type = packet_type;
        p.instruction = instruction;
        p.data = data;

        return p;
    }

    static Packet fromString(QString& data) {
        QStringList components = data.split(':');
        Packet p;

        if (components.length() < 5) {
            throw std::runtime_error("Invalid packet received!");
        }

        p.conversationId = components[0];
        p.source = components[1].toInt();
        p.destination = components[2].toInt();
        p.type = components[3].toInt();
        p.instruction = components[4].toInt();

        // 6 means data is attached.
        if (components.length() == 6) {
            p.data = components[5];
        } else {
            p.data = "";
        }

        return p;
    }

    QString toString() {
        return conversationId
                + ':' + QVariant(source).toString()
                + ':' + QVariant(destination).toString()
                + ':' + QVariant(type).toString()
                + ':' + QVariant(instruction).toString()
                + ':' + data;
    }

private:
    static QString createConversationId() {
        return QUuid::createUuid().toString().replace(QRegExp("[-{}]"), "");
    }
};

Q_DECLARE_METATYPE(Packet)

#endif // PACKET_H
