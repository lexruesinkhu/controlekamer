#include "server.h"

#include <QMessageBox>
#include <QDebug>

QString Server::getClientIp(int id)
{
    Packet p = Packet::make(0, PacketType::STATE_FETCH, StateFetch::GET_IP, QString::number(id));
    auto response = sendAndWait(p);

    return response.data;
}

QList<Client> Server::getClients()
{
    Packet p = Packet::make(0, PacketType::STATE_FETCH, StateFetch::GET_CLIENTS);
    auto response = sendAndWait(p);
    QList<Client> clients;

    auto components = response.data.trimmed().split(",");

    for (auto& component : components) {
        if (component == "") {
            continue;
        }

        auto split = component.split(";");
        Client client;
        client.id = split[1].toInt();
        client.ip = split[2];
        clients.append(client);
    }

    return clients;
}

QList<Event> Server::getEvents()
{
    int amount = 50;
    int offset = 0;

    QString data = QString::number(amount) + ';' + QString::number(offset);

    Packet p = Packet::make(0, PacketType::STATE_FETCH, StateFetch::GET_EVENTS, data);
    auto response = sendAndWait(p);

    QList<Event> events;
    auto components = response.data.trimmed().split(",");

    // TODO: multiple fetches get more than 50 events
    for (auto& component : components) {
        if (component == "") {
            continue;
        }

        events.append(Event::fromString(component));
    }

    return events;
}

void Server::setup()
{
    writeQueue = QQueue<Packet>();
    readDict = QMap<QString, Packet*>();

    socket = new QTcpSocket(this);
    connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
            this, &Server::displayError);

    socket->connectToHost(server, 2500);

    if (! socket->waitForConnected(1000)) {
        QString body = QString("Could not connect to server at ") + server + QString(": \n") + socket->errorString();
        QMessageBox::critical(0, QString("Could not connect to server!"), body);

        socket->deleteLater();

        // We will quit after the message box closes.
        emit errorOccurred();
        return;
    }

    connect(socket, &QTcpSocket::disconnected, [&]() {
        socket->deleteLater();
    });

    // Set up read handler
    connect(socket, &QTcpSocket::readyRead, [&]() {
        auto data = QString(socket->readAll());
        auto packet = Packet::fromString(data);

        // We need to lock here to prevent concurrent
        // access to the readDict map or events list,
        // since these are not thread-safe.
        mutex.lock();

        if (readDict.contains(packet.conversationId)) {
            readDict[packet.conversationId] = new Packet(packet);
        } else {
            emit networkEvent(packet);
        }

        mutex.unlock();
    });

    connect(this, SIGNAL(dataWriteQueued()), this, SLOT(writeDataToSocket()));    
}

void Server::startRegistration()
{
    Packet p = Packet::make(0, PacketType::CONTROL, ControlType::REGISTER_CONTROLLER);
    sendAndWait(p);

    emit registeredWithServer();
}

void Server::writeDataToSocket()
{
    mutex.lock();
    while (! writeQueue.empty()) {
        auto packet = writeQueue.dequeue();
        auto data = packet.toString().toUtf8();

        socket->write(data);
    }
    mutex.unlock();
}

Packet Server::sendAndWait(Packet packet)
{
    //mutex.lock();
    writeQueue.append(packet);
    //mutex.unlock();

    emit dataWriteQueued();

    return waitOnConversation(packet.conversationId);
}

Packet Server::waitOnConversation(QString conversationId)
{
    //mutex.lock();
    readDict[conversationId] = nullptr;
    //mutex.unlock();

    while (true) {
        //mutex.lock();

        if (readDict[conversationId] != nullptr) {
            auto packetPtr = readDict[conversationId];
            Packet packet(*packetPtr);
            delete packetPtr;
            readDict.remove(conversationId);
            return packet;
        }

        //mutex.unlock();

        QThread::usleep(25); // 25ms
    }
}

void Server::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
      case QAbstractSocket::RemoteHostClosedError:
          break;
      case QAbstractSocket::HostNotFoundError:
          QMessageBox::information(0, tr("Controlekamer"),
                                   tr("The host was not found. Please check the "
                                      "host name and port settings."));
          break;
      case QAbstractSocket::ConnectionRefusedError:
          QMessageBox::information(0, tr("Controlekamer"),
                                   tr("The connection was refused by the peer. "
                                      "Make sure the server is running, "
                                      "and check that the host name and port "
                                      "settings are correct."));
          break;
      default:
          QMessageBox::information(0, tr("Controlekamer"),
                                   tr("The following error occurred: %1.")
                                   .arg(socket->errorString()));
      }
}
