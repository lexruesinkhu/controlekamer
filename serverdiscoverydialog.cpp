#include "serverdiscoverydialog.h"
#include "ui_serverdiscoverydialog.h"

#include <QtNetwork/QHostAddress>
#include <QtNetwork/QNetworkDatagram>
#include <QByteArray>
#include <QTimer>
#include <QMessageBox>

ServerDiscoveryDialog::ServerDiscoveryDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ServerDiscoveryDialog)
{
    ui->setupUi(this);

    connect(ui->connectButton, SIGNAL(clicked()), this, SLOT(chooseServer()));
    connect(ui->serverList, SIGNAL(itemSelectionChanged()), this, SLOT(enableConnectButton()));

    socket = new QUdpSocket(this);
    connect(socket, SIGNAL(readyRead()), this, SLOT(receiveDatagramResponse()));
    socket->bind(QHostAddress::Any);
    broadcastDatagram();
}

ServerDiscoveryDialog::~ServerDiscoveryDialog()
{
    delete ui;
}

void ServerDiscoveryDialog::broadcastDatagram()
{
    if (found) {
        return;
    }

    // Discover messages are empty
    QByteArray datagram = "";
    socket->writeDatagram(datagram, QHostAddress::Broadcast, 2501);

    QTimer::singleShot(1000, this, SLOT(broadcastDatagram()));
}

void ServerDiscoveryDialog::receiveDatagramResponse()
{
    QNetworkDatagram datagram = socket->receiveDatagram();
    found = true;

    QString ip = datagram.senderAddress().toString();
    ui->serverList->addItem(ip);
    socket->close();
}

void ServerDiscoveryDialog::chooseServer()
{
    // Store the IP that has been chosen by the user.
    chosenIp = ui->serverList->currentItem()->text();

    // Hide the current window, which gives control back to the main window.
    setVisible(false);
}

void ServerDiscoveryDialog::enableConnectButton()
{
    ui->connectButton->setEnabled(true);
}
