#ifndef CLIENTMANAGER_H
#define CLIENTMANAGER_H

#include <QList>

#include "client.h"

class ClientManager {
public:
    static ClientManager &getInstance() {
        static ClientManager cm;

        return cm;
    }

    void setClients(QList<Client> clients) {
        clients_ = clients;
    }

    QList<Client> &getClients() {
        return clients_;
    }

    Client &getClient(int id) {
        for (auto& client: clients_) {
            if (client.id == id) {
                return client;
            }
        }
    }

    ClientManager(ClientManager const&) = delete;
    void operator=(ClientManager const&) = delete;
private:
    ClientManager() = default;
    QList<Client> clients_;
};

#endif // CLIENTMANAGER_H
