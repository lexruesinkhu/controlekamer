#ifndef SERVER_H
#define SERVER_H

#include <QQueue>
#include <QMap>
#include <QTcpSocket>
#include <QMutex>
#include <QThread>
#include <QDataStream>

#include "event.h"
#include "packet.h"
#include "client.h"

class ServerThread : public QThread
{
    Q_OBJECT
public:
    void run() override
    {
        exec();
    }
};

class Server : public QObject
{
    Q_OBJECT

public:
    Server(QObject *parent = 0) : QObject(parent) { }
    ~Server()
    {
        socket->close();
        delete socket;
    }

public:
    void setServer(QString server)
    {
        this->server = server;
    }

    void stop()
    {
        this->shouldStop = true;
    }

    void setup();

    QString getClientIp(int id);
    QList<Client> getClients();
    QList<Event> getEvents();

signals:
    void networkEvent(Packet packet);
    void dataWriteQueued();
    void eventReceived();
    void errorOccurred();
    void registeredWithServer();

public slots:
    void startRegistration();

private slots:
    void writeDataToSocket();

private:
    Packet sendAndWait(Packet packet);
    Packet waitOnConversation(QString conversationId);    
    void displayError(QAbstractSocket::SocketError socketError);

    QMutex mutex;

    QMap<QString, Packet*> readDict;
    QQueue<Packet> writeQueue;

    QTcpSocket *socket;
    QString server;

    bool shouldStop = false;
};

#endif // SERVER_H
