#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "serverdiscoverydialog.h"
#include "clientmanager.h"

#include <QObject>
#include <QMessageBox>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{  
    ui->setupUi(this);
    ui->statusBar->showMessage("Waiting for connection dialog...");

    qRegisterMetaType<Packet>();

    emergency = new QLabel(this);
    emergency->setText("No emergency");

    connect(ui->actionStop_emergency, SIGNAL(triggered(bool)), this, SLOT(resetEmergency()));

    // This places the emergency label above the events list
    rightLayout = new QVBoxLayout(this);
    rightLayout->addWidget(emergency);
    rightLayout->addWidget(ui->eventList);

    this->ui->gridLayout->addLayout(rightLayout, 0, 2);

    QTimer::singleShot(0, this, SLOT(startDiscoveryDialog()));

    connect(ui->actionRefresh_rooms, SIGNAL(triggered(bool)), this, SLOT(fetchClients()));

    for (int row = 0; row < 3; row++) {
        for (int col = 0; col < 2; col++) {
            auto view = new ClientView(this);
            ui->clientViewGrid->addWidget(view, row, col);
        }
    }
}

MainWindow::~MainWindow()
{    
    /*
     * If the discovery dialog is canceled, the
     * server thread will not exist. Trying to exit would
     * result in a segfault.
     */
    if (serverThread && serverThread->isRunning()) {
        serverThread->exit();
    }

    delete ui;
}

void MainWindow::changeStatusMessage(QString text)
{
    ui->statusBar->showMessage(text);
}

void MainWindow::resetEmergency()
{
    emergency->setText("No emergency");
}

void MainWindow::processNetworkEvent(Packet packet)
{
    if (packet.type != PacketType::STATE_CHANGE) {
        return;
    }

    Event event(packet.instruction, packet.source);

    switch(packet.instruction) {
        case StateChange::START_EMERGENCY: {
            emergency->setText("!!! EMERGENCY ACTIVE !!!");
            break;
        }
    }

    addEventToList(event);
}

void MainWindow::startDiscoveryDialog()
{
    discoveryDialog = new ServerDiscoveryDialog(this);
    connect(discoveryDialog, SIGNAL(rejected()), this, SLOT(close()));

    /*
     * If the discovery dialog is rejected, the code path after ->exec() will
     * still run. This would mean that we try to connect to the server before we
     * shut down. We keep a boolean to make sure that doesn't happen.
     * Since this is an event and ->exec() runs all events before returning, this is guaranteed
     * to execute before continuing the code path
     */
    connect(discoveryDialog, &ServerDiscoveryDialog::rejected, [&]() {
        discoveryRejected = true;
    });

    discoveryDialog->exec();

    if (discoveryRejected) {
        return;
    }

    changeStatusMessage("Connecting with " + discoveryDialog->serverIp() + "...");

    // Setup server connection
    server = new Server();
    server->setServer(discoveryDialog->serverIp());

    serverThread = new ServerThread();

    connect(server, SIGNAL(registeredWithServer()), this, SLOT(fetchClients()));
    connect(server, SIGNAL(registeredWithServer()), this, SLOT(fetchEvents()));
    connect(server, SIGNAL(networkEvent(Packet)), this, SLOT(processNetworkEvent(Packet)));

    serverThread->start();

    /*
     * We need to setup before moving to the other thread,
     * otherwise there will be problems, since we define server in this thread
     * but try to setup handles en objects in another.
     *
     * ->moveToThread(...) handles this for us.
     */
    server->setup();
    server->moveToThread(serverThread);
    server->startRegistration();
}

void MainWindow::fetchClients()
{
    auto clients = server->getClients();

    ClientManager::getInstance().setClients(clients);

    ui->clientList->clear();

    for (auto& client : clients) {
        QString name = "Room " + QString::number(client.id);
        ui->clientList->addItem(name);
    }
}

void MainWindow::fetchEvents()
{
    auto events = server->getEvents();

    ui->eventList->clear();

    for (auto& event : events) {
        addEventToList(event);
    }
}

QString MainWindow::createEventText(Event &event)
{
    QString text = "Room " + QString::number(event.room_nr);

    switch(event.event_type) {
        case StateChange::LIGHT_TURNED_ON: {
            text += " light turned on";
            break;
        }

        case StateChange::LIGHT_TURNED_OFF: {
            text += " light turned off";
            break;
        }

        case StateChange::CAMERA_ENABLED: {
            text += " camera turned on";
            break;
        }

        case StateChange::CAMERA_DISABLED: {
            text += " camera turned off";
            break;
        }

        case StateChange::START_EMERGENCY: {
            text += " emergency!";
            break;
        }
    }

    return text;
}

void MainWindow::addEventToList(Event &event)
{
    auto text = createEventText(event);
    ui->eventList->insertItem(0, text);
}
