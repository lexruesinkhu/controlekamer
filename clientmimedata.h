#ifndef CLIENTMIMEDATA_H
#define CLIENTMIMEDATA_H

#include <QMimeData>

#include "client.h"

class ClientMimeData : public QMimeData
{
public:
    ClientMimeData(const Client client)
        : client(std::move(client) { }

    Client client;
};

#endif // CLIENTMIMEDATA_H
