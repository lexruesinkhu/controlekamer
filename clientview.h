#ifndef CLIENTVIEW_H
#define CLIENTVIEW_H

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QWidget>
#include <QLabel>
#include <QTreeView>
#include <QGridLayout>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QTreeWidgetItem>
#include <QVariant>
#include <QUdpSocket>
#include <QTimer>
#include <QPixmap>
#include <QPushButton>

#include "clientmanager.h"

class ClientView : public QWidget
{
    Q_OBJECT

public:
    explicit ClientView(QWidget *parent = nullptr)
        : QWidget(parent)
    {
        socket = new QUdpSocket(this);
        timer = new QTimer(this);

        connect(timer, &QTimer::timeout, [&]() {
            if (socket->isOpen()) {
                socket->write("1", 1);
            }
        });

        display = new QLabel(this);
        list = new QTreeWidget(this);
        close = new QPushButton(this);

        close->setText("Close");

        layout = new QGridLayout(this);
        rightLayout = new QVBoxLayout(this);

        rightLayout->addWidget(list);
        rightLayout->addWidget(close);

        connect(socket, &QUdpSocket::readyRead, [&]() {
            auto data = socket->readAll();

            QPixmap pixelMap;
            pixelMap.loadFromData(QByteArray::fromBase64(data), "JPG");

            if (! pixelMap.isNull()) {
                pixelMap.scaled(display->width(), display->height(), Qt::KeepAspectRatio);
                display->setPixmap(pixelMap);
            }
        });

        connect(socket, &QUdpSocket::connected, [&]() {
            if (! active) {
                return;
            }

            if (! timer->isActive()) {
                timer->start(1000 / 8); // 8fps
            }

            // The client simply needs to know we want a frame,
            // we don't need to send any data in the packet.
            if (socket->isOpen()) {
                socket->write("1", 1);
            }
        });

        connect(socket, &QUdpSocket::disconnected, [&]() {
            connectToClient();
        });

        connect(close, SIGNAL(released()), this, SLOT(closeRoom()));

        resetView();

        layout->addWidget(display, 0, 0);
        layout->addLayout(rightLayout, 0, 1);

        setAcceptDrops(true);
    }

private slots:
   void closeRoom()
   {
       active = false;

       timer->stop();
       socket->close();

       resetView();
   }

protected:
    void connectToClient()
    {
        if (active) {
            auto client = ClientManager::getInstance().getClient(clientId);
            socket->connectToHost(client.ip, 2502);
        }
    }

    void resetView()
    {
        QPalette palette;

        palette.setColor(QPalette::Background, Qt::black);

        display->clear();
        display->setMinimumWidth(300);
        display->setAutoFillBackground(true);
        display->setPalette(palette);

        close->setDisabled(true);
        list->headerItem()->setText(0, "Empty");
    }

    void dragEnterEvent(QDragEnterEvent *event) override
    {
       setBackgroundRole(QPalette::Highlight);
       event->acceptProposedAction();
    }

    void dragMoveEvent(QDragMoveEvent *event) override
    {
        event->acceptProposedAction();
    }

    void dropEvent(QDropEvent *event) override
    {
        const auto* mimeData = event->mimeData();

        auto data = mimeData->data("application/x-qabstractitemmodeldatalist");
        QDataStream stream(&data, QIODevice::ReadOnly);

        QMap<int, QVariant> roleDataMap;

        while (!stream.atEnd()) {
            int row, col;
            stream >> row >> col >> roleDataMap;
        }

        // RoleDataMap will have only one item!
        // The value will be the text that was set in the list item.
        QString headerText = roleDataMap.value(0).toString();

        list->headerItem()->setText(0, headerText);
        auto split = headerText.split(" ");
        clientId = split[1].toInt();

        event->acceptProposedAction();

        active = true;
        close->setDisabled(false);

        connectToClient();
    }

    void dragLeaveEvent(QDragLeaveEvent *event) override
    {
        event->accept();
    }

private:
    bool active = false;

    int clientId;

    QUdpSocket *socket;
    QTimer *timer;

    QLabel *display;
    QPushButton *close;
    QTreeWidget *list;
    QGridLayout *layout;
    QVBoxLayout *rightLayout;
};

#endif // CLIENTVIEW_H
